package com.movies.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.movies.request.MovieDetailsRequest;

public class MoviesJdbc {

	public boolean InsertMovieDetails(MovieDetailsRequest movieDetailsRequest) {
		boolean recordInserted = false;
		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "insert into movie_details (movie_name,movie_release_year) values (?,?)";

			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, movieDetailsRequest.getMovieName());
			statement.setString(2, movieDetailsRequest.getMovieReleaseYear());

			int count = statement.executeUpdate();
			if (count == 1) {
				recordInserted = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordInserted;

	}

	public ArrayList<MovieDetailsRequest> getMovieDetails() {
	
		ArrayList<MovieDetailsRequest> list = new ArrayList<>();
		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "select * from movie_details";

			PreparedStatement statement = connection.prepareStatement(query);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				MovieDetailsRequest movieDetailsRequest = new MovieDetailsRequest();
				String movieName = rs.getString("movie_name");
				String movieReleaseYear = rs.getString("movie_release_year");
				movieDetailsRequest.setMovieName(movieName);
				movieDetailsRequest.setMovieReleaseYear(movieReleaseYear);
				
				list.add(movieDetailsRequest);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
