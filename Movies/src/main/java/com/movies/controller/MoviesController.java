package com.movies.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.movies.jdbc.MoviesJdbc;
import com.movies.movieResponse.StatusResponse;
import com.movies.request.MovieDetailsRequest;
import com.movies.request.MovieListRequest;

@RestController
public class MoviesController {

	@PostMapping("/movie-details")
	public StatusResponse insertMovieDetails(@RequestBody MovieDetailsRequest movieDetailsRequest) {
		MoviesJdbc moviesJdbc = new MoviesJdbc();
		boolean insertedDetails = moviesJdbc.InsertMovieDetails(movieDetailsRequest);
		StatusResponse statusResponse = new StatusResponse();
		if (insertedDetails == true) {
			statusResponse.setStatus("success");
			statusResponse.setStatusText("record inserted succesfully");
		} else {
			statusResponse.setStatus("failed");
			statusResponse.setStatusText("failed to insert details");
		}
		return statusResponse;
	}
	
	@GetMapping("/movies")
	public MovieListRequest getMovieDetails() {
		
		MoviesJdbc moviesJdbc = new MoviesJdbc();
		ArrayList<MovieDetailsRequest> moviesDetails = moviesJdbc.getMovieDetails();
		MovieListRequest movieListRequest = new MovieListRequest();
		if(moviesDetails != null && !moviesDetails.isEmpty()) {
			movieListRequest.setStatus("success");
			movieListRequest.setMovieDetails(moviesDetails);
			
		}else
		{
			movieListRequest.setStatus("failed");
		}
		return movieListRequest;
	}
}
